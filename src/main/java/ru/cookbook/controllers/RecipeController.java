package ru.cookbook.controllers;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RecipeController {

    /**
     * Вывод рецепта на экран
     * @return json с рецептами
     */
    @RequestMapping("/reciept/{id}")
    public String getReciept(@PathVariable("id") int id){
        //TODO вывод рецепты на экран
        return "Рецептик" + id;
    }

    /**
     * Добавить рецепт
     * @return статус сохрарнеия рецепта
     */
    @RequestMapping("/recieptadd")
    public String addReciept(){
        //TODO добавить рецепт
        return "Рецептик добавлен";
    }

    /**
     * Удаление рецепта
     * @return статус удаления
     */
    @RequestMapping("/recieptdelete")
    public String deleteReciept(){
        //TODO удалить рецепт пометь как неиспользуемый
        return "Рецептик удален";
    }

    /**
     * изменение рецепта
     * @return статус изменения
     */
    @RequestMapping("/recieptchange")
    public String changeReciept(){
        //TODO ихменение рецепта
        return "Рецептик изменен";
    }
}
